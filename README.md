# Algorand Wallet

The Algorand Wallet is a chrome extension that enhances the Algorand ecosystem by enabling safe interactions with decentralized applications (Dapps).

Allowing users to safely provide their account details to Dapps creates space for innovative products to emerge that showcase Algorand's superior blockchain technology.

Much like Metamask, the chrome extension that enabled an ecosystem to flourish on the Ethereum network, Algorand Wallet will unlock the peripheral value and network effects afforded by a strong developer community.

## Safety Features

### No Central Server

All user information is contained within the safety of the extension's background script environment. Sensitive information is encrypted with a strong password and stored locally on the user's machine.

### Permissions

The default privacy of the wallet is to provide no information about the user's account to Dapps without explicit user consent. A permission system allows users to provide their information to Dapps on a case-by-case basis.

### Clickjacking Safe

The wallet is not accessible from the wider web and is not vulnerable to clickjacking attacks.

## Wallet Features

- [x] Passphrase generation and verification game
- [x] Passphrase recovery
- [x] Password encypted passphrase
- [x] QRCode for deposits (compatible with the official Algorand mobile wallet)
- [x] Deposit ALGO
- [x] Send ALGO
- [x] Paginated transaction list
- [x] Transaction details page
- [x] Easy/quick one-click copying of important details
- [x] Permission system
- [x] Account injection and interactions for Dapps
- [x] Transaction prompts from Dapps
- [x] Browser notifications on successful transactions
- [x] Wallet logout

## Installation

The extension is available to download & install from the [Google Chrome Store](https://chrome.google.com/webstore/detail/algorand-wallet/fdahclgkkbnjkfcngfbohobpmedlcfpm).

Alternatively, you can build the extension locally and install it as an unpacked extension following the steps below:

1. clone this repo
2. `npm install`
3. `npm run build`
4. Open up [Manage Extensions](https://support.google.com/chrome_webstore/answer/2664769?hl=en) in your Chrome browser, click "Load unpacked", and select the project build folder.
5. Click the extension icon in the top right of your browser to open up the wallet.

## Example Dapp

A simple chat application has been created to showcase the features of the wallet. Once you have installed the wallet using the instructions above you can visit and interact with the [example chat application](https://algo-example.now.sh).
