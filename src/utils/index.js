import BigNumber from "bignumber.js";

export const shuffle = arr =>
  arr
    .map(a => ({ sort: Math.random(), value: a }))
    .sort((a, b) => a.sort - b.sort)
    .map(a => a.value);

export const ordinal = i => {
  var j = i % 10,
    k = i % 100;
  if (j == 1 && k != 11) {
    return i + "st";
  }
  if (j == 2 && k != 12) {
    return i + "nd";
  }
  if (j == 3 && k != 13) {
    return i + "rd";
  }
  return i + "th";
};

export const sendMessage = payload =>
  new Promise((resolve, reject) => {
    window.chrome.runtime.sendMessage(payload, (response, err) => {
      if (err) {
        return reject(err);
      }
      return resolve(response);
    });
  });

export const fromMicro = micro => {
  // The minimum divisible unit of an Algo is a microAlgo (10^-6).
  // https://www.algorand.com/resources/blog/rewards-technical-overview
  const decimals = new BigNumber(6);
  const divisor = new BigNumber(10).pow(decimals);
  return new BigNumber(micro).div(divisor);
};

export const toMicro = algo => {
  const decimals = new BigNumber(6);
  const divisor = new BigNumber(10).pow(decimals);
  return new BigNumber(algo).times(divisor);
};

export const formatMicro = micro =>
  fromMicro(micro)
    .toNumber()
    .toLocaleString();

export const truncate = str =>
  `${str.slice(0, 6)}...${str.slice(str.length - 6, str.length)}`;

export const validPassword = password => {
  // 1 lowercase character, 1 uppercase character, 1 numeric character, min 8 characters
  const validator = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
  if (validator.test(password)) {
    return true;
  }
  return false;
};

export const validPasswords = (password, confirmPassword) => {
  if (validPassword(password) && password === confirmPassword) {
    return true;
  }
  return false;
};

export const generateAccount = async () => {
  const response = await sendMessage({
    type: "GENERATE_ACCOUNT"
  });
  return response;
};

export const createAccount = async (mnemonic, password) => {
  const response = await sendMessage({
    type: "CREATE_ACCOUNT",
    mnemonic: mnemonic,
    password: password
  });
  return response;
};

export const getAccount = async () => {
  const response = await sendMessage({ type: "ACCOUNT_INFO" });
  return response;
};

export const randomFromPassphrase = mnemonic => {
  const words = mnemonic.split(" ");
  const randomIndex = Math.floor(Math.random() * words.length);
  const randomWord = words[randomIndex];
  return {
    randomIndex,
    randomWord
  };
};

export const verifyWord = (word, expectedWord, rounds) => {
  const minRounds = 3;
  if (word === expectedWord) {
    if (rounds < minRounds) {
      return false;
    } else {
      return true;
    }
  }
};

export const deselect = () => {
  if (window.getSelection) {
    window.getSelection().removeAllRanges();
  } else if (document.selection) {
    document.selection.empty();
  }
};
