import React from "React";

export default WrappedComponent =>
  class extends React.Component {
    render() {
      return (
        <div style={{ padding: "20px 16px", width: 360, height: 600 }}>
          <WrappedComponent {...this.props} />
        </div>
      );
    }
  };
