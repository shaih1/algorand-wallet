import React from "React";

export default ({ children, className, direction, style }) => (
  <div
    className={className}
    style={{
      display: "flex",
      justifyContent: "center",
      flexDirection: direction || "column",
      alignItems: "center",
      overflowWrap: "break-word",
      ...style
    }}
  >
    {children}
  </div>
);
