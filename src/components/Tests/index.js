import React from "react";
import "mocha/mocha.js";
import "mocha/mocha.css";
import chai from "chai/chai.js";
import algosdk from "algosdk";

import {
  sendMessage,
  validPassword,
  validPasswords,
  generateAccount,
  randomFromPassphrase,
  verifyWord,
  createAccount,
  getAccount
} from "../../utils";

const mocha = window.mocha;

window.chai = chai;
const expect = chai.expect;

class Tests extends React.Component {
  componentDidMount() {
    this.tests();
  }

  tests = () => {
    mocha.setup("bdd");
    mocha.checkLeaks();

    describe("validPassword", () => {
      it("password should be rejected", () => {
        const password = "test1234";
        expect(validPassword(password)).to.equal(false);
      });

      it("password should be accepted", () => {
        const password = "Test1234";
        expect(validPassword(password)).to.equal(true);
      });
    });

    describe("validPasswords", () => {
      it("confirmPassword should be rejected", () => {
        const password = "Test1234";
        const confirmPassword = "test1234";
        expect(validPasswords(password, confirmPassword)).to.equal(false);
      });
      it("confirmPassword should be accepted", () => {
        const password = "Test1234";
        const confirmPassword = "Test1234";
        expect(validPasswords(password, confirmPassword)).to.equal(true);
      });
    });

    describe("generateAccount", () => {
      it("should generate a valid mnemonic", async () => {
        const { mnemonic } = await generateAccount();
        const tempAccount = algosdk.mnemonicToSecretKey(mnemonic);
        const valid = algosdk.isValidAddress(tempAccount.addr);
        expect(valid).to.equal(true);
      });
    });

    describe("randomFromPassphrase / verifyWord", () => {
      it("should verify a mnemonic", async () => {
        const { mnemonic } = await generateAccount();
        let rounds = 0;
        let verified = false;

        const r1 = randomFromPassphrase(mnemonic);
        rounds += 1;
        verified = verifyWord(r1.randomWord, r1.randomWord, rounds);
        expect(verified).to.equal(false);

        const r2 = randomFromPassphrase(mnemonic);
        rounds += 1;
        verified = verifyWord(r2.randomWord, r2.randomWord, rounds);
        expect(verified).to.equal(false);

        const r3 = randomFromPassphrase(mnemonic);
        rounds += 1;
        verified = verifyWord(r3.randomWord, r3.randomWord, rounds);

        expect(verified).to.equal(true);
      });
    });

    describe("createAccount", () => {
      it("should create an account with passphrase and password", async () => {
        const { mnemonic } = await generateAccount();
        const password = "Test1234";
        const { loggedIn } = await createAccount(mnemonic, password);
        console.log(loggedIn);
        expect(loggedIn).to.equal(true);
        const account = await getAccount();
        console.log(account);
        // new accounts have a 0 balance
        expect(account.amount).to.equal(0);
      }).timeout(15000);
    });

    mocha.run();
  };

  render() {
    return <div id="mocha" />;
  }
}

export default Tests;
