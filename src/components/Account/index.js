import React from "react";
import formatDistanceStrict from "date-fns/formatDistanceStrict";
import classNames from "classnames";
import InfiniteScroll from "react-infinite-scroller";
import QRCode from "qrcode";
import algosdk from "algosdk";

import Center from "../Center";
import Copyable from "../Copyable";
import Loader from "../Loader";

import {
  sendMessage,
  fromMicro,
  toMicro,
  formatMicro,
  truncate,
  getAccount
} from "../../utils";

import logo from "../../images/logo.jpg";

class Account extends React.Component {
  state = {
    loading: true,
    selected: "",
    transactions: [],
    hasMore: true,
    page: "account"
  };

  componentDidMount() {
    this.load();
  }

  load = async () => {
    const cache = await sendMessage({ type: "CACHED_ACCOUNT_INFO" });
    if (cache) {
      this.setState({ ...cache, loading: false });
    }
    const response = await getAccount();
    this.setState({ ...response, loading: false });
  };

  loadMore = async () => {
    const transactions = this.state.transactions;
    if (transactions.length) {
      const round = transactions[transactions.length - 1].round;
      const response = await sendMessage({
        type: "LOAD_MORE_TRANSACTIONS",
        round
      });
      if (response.length) {
        await this.setState({ transactions: [...transactions, ...response] });
      }
    }

    // if the number of transactions is the same after fetching more, stop fetching
    if (transactions.length === this.state.transactions.length) {
      this.setState({ hasMore: false });
    }
  };

  render() {
    const {
      page,
      loading,
      address,
      amount,
      transactions,
      selected
    } = this.state;
    if (loading) {
      return <Loader />;
    }

    if (page === "deposit") {
      return (
        <Center>
          <div className="mb-3">Deposit</div>
          <Copyable value={address}>{truncate(address)}</Copyable>
          <img src={this.state.qrcode} />
          <button
            type="button"
            className="btn btn-primary w-100"
            onClick={() => this.setState({ page: "account" })}
          >
            Back
          </button>
        </Center>
      );
    }

    if (page === "send") {
      return (
        <Center>
          <div className="mb-3">Send</div>
          <div className="input-group flex-nowrap">
            <div className="input-group-prepend" style={{ display: "block" }}>
              <span className="input-group-text" style={{ padding: 0 }}>
                <img
                  src={logo}
                  style={{ width: 36, height: 36, borderRadius: ".25rem" }}
                />
              </span>
            </div>
            <input
              type="number"
              step="0.000001"
              className={classNames("form-control mb-3")}
              placeholder="Amount"
              value={this.state.sendingAmount}
              onChange={e => {
                this.setState({
                  sendingAmount: Number(e.target.value).toFixed(6)
                });
              }}
            />
            <div
              style={{ display: "block", cursor: "pointer" }}
              className="input-group-append"
              onClick={() => {
                console.log(fromMicro(amount).toString());
                // 0.1 is minimum required balanace without specifiying a close account
                // 0.001 is a standard fee amount
                // TODO: deal with close accounts
                const minus = 0.101;
                this.setState({
                  sendingAmount: fromMicro(amount)
                    .minus(minus)
                    .toString()
                });
              }}
            >
              <span className="input-group-text">max</span>
            </div>
          </div>
          <input
            type="text"
            className={classNames("form-control mb-3", {
              "is-valid": algosdk.isValidAddress(this.state.sendingTo)
            })}
            placeholder="To"
            value={this.state.sendingTo}
            onChange={e => this.setState({ sendingTo: e.target.value })}
          />
          <button
            disabled={!algosdk.isValidAddress(this.state.sendingTo)}
            type="button"
            className="btn btn-primary w-100 mb-3"
            onClick={() => this.setState({ page: "preview" })}
          >
            Preview
          </button>
          <button
            type="button"
            className="btn btn-primary w-100"
            onClick={() => this.setState({ page: "account" })}
          >
            Back
          </button>
        </Center>
      );
    }

    if (page === "preview") {
      return (
        <Center>
          <div className="mb-3">Transaction Details</div>
          <div className="text-muted">Amount</div>
          <div className="mb-3">{this.state.sendingAmount}</div>
          <div className="text-muted">To</div>
          <div className="mb-3">
            <Copyable value={this.state.sendingTo}>
              {truncate(this.state.sendingTo)}
            </Copyable>
          </div>
          <div className="text-muted">Fee</div>
          <div className="mb-3">0.001</div>
          <button
            type="button"
            className="btn btn-primary w-100 mb-3"
            onClick={async () => {
              await sendMessage({
                type: "SEND_TRANSACTION",
                to: this.state.sendingTo,
                amount: toMicro(this.state.sendingAmount).toNumber()
              });
              this.setState({ page: "account" });
            }}
          >
            Send
          </button>
          <button
            type="button"
            className="btn btn-primary w-100"
            onClick={() => this.setState({ page: "account" })}
          >
            Back
          </button>
        </Center>
      );
    }

    if (page === "account") {
      if (selected) {
        return (
          <Center>
            <div className="mb-3">Transaction Details</div>
            <div className="text-muted">Amount</div>
            <div className="mb-3">
              {selected.payment.to === address ? (
                <span className="text-success">
                  +{formatMicro(selected.payment.amount)}
                </span>
              ) : (
                <span className="text-danger">
                  -{formatMicro(selected.payment.amount)}
                </span>
              )}
            </div>
            <div className="text-muted">Sending Address</div>
            <div className="mb-3">
              <Copyable value={selected.from}>
                {truncate(selected.from)}
              </Copyable>
            </div>
            <div className="text-muted">Receiving Address</div>
            <div className="mb-3">
              <Copyable value={selected.payment.to}>
                {truncate(selected.payment.to)}
              </Copyable>
            </div>
            <div className="text-muted">Transaction ID</div>
            <div className="mb-3">
              <Copyable value={selected.tx}>{truncate(selected.tx)}</Copyable>
            </div>
            <div className="text-muted">Fee</div>
            <div className="mb-3">{formatMicro(selected.fee)}</div>
            <button
              type="button"
              className="btn btn-primary w-100"
              onClick={() => this.setState({ selected: null })}
            >
              Back
            </button>
          </Center>
        );
      }
      return (
        <Center>
          <div
            className="w-100"
            style={{ display: "flex", justifyContent: "flex-end" }}
          >
            <div onClick={this.props.logout}>
              <i
                style={{ cursor: "pointer" }}
                className="fas fa-power-off text-muted"
              ></i>
            </div>
          </div>

          <div className="mb-3">Algos Available</div>
          <Center direction="row" className="mb-3">
            <img src={logo} style={{ width: 25 }} alt="logo" />
            {formatMicro(amount)}
          </Center>
          <Center style={{ flexDirection: "row" }}>
            <button
              type="button"
              className="btn btn-primary mb-4 mr-4"
              onClick={async () => {
                // this is the QR Code format the official algorand wallet uses
                const qrcode = await QRCode.toDataURL(
                  JSON.stringify({
                    address: address,
                    version: "1.0",
                    amount: 0
                  })
                );
                this.setState({ page: "deposit", qrcode });
              }}
            >
              Deposit
            </button>
            <button
              type="button"
              className="btn btn-primary mb-4 ml-4"
              onClick={() =>
                this.setState({
                  page: "send",
                  sendingTo: "",
                  sendingAmount: "0.000000"
                })
              }
            >
              Send
            </button>
          </Center>
          <small className="text-muted w-100 mb-1">Transaction History</small>
          <ul className="list-group w-100 mb-4">
            {!loading && !transactions.length && (
              <li className="list-group-item">
                <div>No Transactions Found</div>
              </li>
            )}
            <InfiniteScroll
              pageStart={0}
              loadMore={this.loadMore}
              hasMore={this.state.hasMore}
              loader={<Loader />}
            >
              {transactions.map(transaction => (
                <li
                  key={transaction.tx}
                  className="list-group-item"
                  style={{
                    cursor: "pointer",
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                  onClick={() => this.setState({ selected: transaction })}
                >
                  <div>{truncate(transaction.from)}</div>
                  <div>
                    {transaction.payment.to === address ? (
                      <span className="text-success">
                        +{formatMicro(transaction.payment.amount)}
                      </span>
                    ) : (
                      <span className="text-danger">
                        -{formatMicro(transaction.payment.amount)}{" "}
                      </span>
                    )}
                  </div>
                </li>
              ))}
            </InfiniteScroll>
          </ul>
        </Center>
      );
    }
  }
}

export default Account;
