import React from "react";
import formatDistanceStrict from "date-fns/formatDistanceStrict";
import classNames from "classnames";
import algosdk from "algosdk";

import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/js/all.js";

import withContainer from "../withContainer";
import Account from "../Account";
import Center from "../Center";
import Copyable from "../Copyable";

import {
  shuffle,
  ordinal,
  sendMessage,
  validPassword,
  validPasswords,
  generateAccount,
  randomFromPassphrase,
  verifyWord,
  deselect,
  createAccount
} from "../../utils";

import logo from "../../images/logo.jpg";

class App extends React.Component {
  state = {
    loading: true,
    password: "",
    confirmPassword: "",
    address: null,
    page: "",
    question: 0
  };

  componentDidMount() {
    this.run();
  }

  run = async () => {
    const response = await sendMessage({ type: "INIT" });
    let page = "";
    if (response.hasAccount) {
      if (response.loggedIn) {
        page = "account";
      } else {
        page = "login";
      }
    }
    this.setState({ ...response, page, loading: false });
  };

  verifyRandom = () => {
    // if user copies text selection it may carry over, deselect everything
    deselect();

    const { randomIndex, randomWord } = randomFromPassphrase(
      this.state.mnemonic
    );
    this.setState({
      randomIndex,
      randomWord,
      page: "verify",
      question: this.state.question + 1
    });
  };

  verifyWord = async word => {
    if (verifyWord(word, this.state.randomWord, this.state.question)) {
      // finish
      const response = await createAccount(
        this.state.mnemonic,
        this.state.password
      );
      this.setState({
        question: 0,
        page: "verified",
        password: "",
        confirmPassword: "",
        mnemonic: "",
        ...response
      });
    } else {
      this.verifyRandom();
    }
  };

  logout = async () => {
    await sendMessage({ type: "LOGOUT" });
    this.setState({ page: "login" });
  };

  render() {
    const {
      loading,
      password,
      confirmPassword,
      address,
      page,
      mnemonic,
      randomIndex
    } = this.state;
    if (loading) {
      return <div />;
    }

    if (page === "choosePassword") {
      return (
        <Center>
          <h4 className="mb-3">Choose a password</h4>
          <input
            type="password"
            className={classNames("form-control mb-3", {
              "is-valid": validPassword(password)
            })}
            placeholder="Password"
            value={password}
            onChange={e => this.setState({ password: e.target.value })}
          />
          <input
            type="password"
            className={classNames("form-control mb-3", {
              "is-valid": validPasswords(password, confirmPassword)
            })}
            placeholder="Confirm Password"
            value={confirmPassword}
            onChange={e => this.setState({ confirmPassword: e.target.value })}
          />
          <small className="mb-3">
            Password must be at least 8 characters long and contain 1 uppercase
            character and 1 numeric character.
          </small>
          <button
            disabled={!validPasswords(password, confirmPassword)}
            type="button"
            className="btn btn-primary w-100"
            onClick={async () => {
              if (validPasswords(password, confirmPassword)) {
                if (mnemonic) {
                  const response = await createAccount(mnemonic, password);
                  this.setState({
                    question: 0,
                    page: "account",
                    password: "",
                    confirmPassword: "",
                    mnemonic: "",
                    ...response
                  });
                } else {
                  const response = await generateAccount();
                  this.setState({
                    page: "create",
                    mnemonic: response.mnemonic
                  });
                }
              }
            }}
          >
            Next
          </button>
        </Center>
      );
    }

    if (page === "create") {
      return (
        <Center>
          <h4 className="mb-3">Back up this Passphrase</h4>
          <div className="mb-3">
            <Copyable value={mnemonic}>
              {mnemonic.split(" ").map((word, index) => (
                <span>{word} </span>
              ))}
            </Copyable>
          </div>
          <div className="alert alert-warning" role="alert">
            If you lose this device you will need this phrase to access your
            account.
          </div>
          <button
            type="button"
            class="btn btn-primary w-100 mb-3"
            onClick={this.verifyRandom}
          >
            Verify Recovery Phrase
          </button>
        </Center>
      );
    }

    if (page === "verify") {
      return (
        <Center>
          <div style={{ marginBottom: 10 }}>
            Select the {ordinal(randomIndex + 1)} word of your passphrase.
          </div>
          <div>
            {shuffle(mnemonic.split(" ")).map(word => (
              <span
                style={{ cursor: "pointer", margin: 5 }}
                class="badge badge-primary"
                onClick={() => this.verifyWord(word)}
              >
                {word}{" "}
              </span>
            ))}
          </div>
        </Center>
      );
    }

    if (page === "login") {
      return (
        <Center>
          <h4 className="mb-3">Enter Password</h4>
          <input
            type="password"
            className={classNames("form-control mb-3", {
              "is-valid": validPassword(password)
            })}
            placeholder="Password"
            value={password}
            onChange={e => this.setState({ password: e.target.value })}
          />
          <button
            disabled={!validPassword(password)}
            type="button"
            className="btn btn-primary w-100"
            onClick={async () => {
              if (validPassword(password)) {
                const response = await sendMessage({
                  type: "LOGIN",
                  password: this.state.password
                });
                if (response.loggedIn) {
                  this.setState({ page: "account", password: "", ...response });
                }
              }
            }}
          >
            Next
          </button>
        </Center>
      );
    }

    if (page === "recover") {
      return (
        <Center>
          <h4 className="mb-3">Recover from Passphrase</h4>
          <textarea
            style={{ height: 130 }}
            type="text"
            class="form-control mb-3"
            placeholder="Enter Passphrase"
            value={mnemonic}
            onChange={e =>
              this.setState({ mnemonic: e.target.value.replace(/\s+/g, " ") })
            }
          />
          <button
            type="button"
            className="btn btn-primary w-100"
            onClick={async () => {
              const response = await sendMessage({
                type: "VALID_MNEMONIC",
                mnemonic
              });
              if (response.isValid) {
                this.setState({ page: "choosePassword" });
              }
            }}
          >
            Next
          </button>
        </Center>
      );
    }

    if (page === "verified") {
      return (
        <Center>
          <div className="mb-3">Passphrase Verified</div>
          <div className="alert alert-warning mb-3" role="alert">
            Your passphrase has been verified. Make sure that you have backed it
            up as you will need it to recover your account.
          </div>
          <button
            type="button"
            class="btn btn-primary w-100"
            onClick={() => this.setState({ page: "account" })}
          >
            OK
          </button>
        </Center>
      );
    }

    if (page === "account") {
      return <Account logout={this.logout} />;
    }

    return (
      <Center>
        <div>
          <img src={logo} style={{ width: 320 }} alt="logo" />
        </div>
        <button
          type="button"
          class="btn btn-primary w-100"
          onClick={() => {
            this.setState({ page: "choosePassword" });
          }}
        >
          Create Account
        </button>
        <small style={{ paddingTop: 16, paddingBottom: 5 }}>
          Already have an account?
        </small>
        <button
          type="button"
          class="btn btn-primary w-100"
          onClick={() => this.setState({ page: "recover" })}
        >
          Recover From Passphrase
        </button>
      </Center>
    );
  }
}

export default withContainer(App);
