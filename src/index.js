import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import App from "./components/App";
import Tests from "./components/Tests";

class Index extends React.Component {
  render() {
    const test = new URL(window.location.href).searchParams.get("test");
    if (test) {
      return <Redirect to="/tests" />;
    }
    return <Redirect to="/" />;
  }
}

ReactDOM.render(
  <Router>
    <div>
      <Route exact path="/index.html" component={Index} />
      <Route exact path="/" component={App} />
      <Route exact path="/tests" component={Tests} />
    </div>
  </Router>,
  document.getElementById("root")
);
