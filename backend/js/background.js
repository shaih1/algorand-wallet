import "@babel/polyfill";
import "whatwg-fetch";
import algosdk from "algosdk";
import crypto from "crypto-js";
import { uuid } from "uuidv4";
import BigNumber from "bignumber.js";

const apiToken =
  "0f24cac92e5ead6afbcf389e0ade28bb609d24ca6687359f342748c68d6cf9b2";
const server = "https://indexer.algorand.network";
const port = 8443;
const algodclient = new algosdk.Algod(apiToken, server, port);

// utilities
const storage = {
  get: key =>
    new Promise(resolve =>
      chrome.storage.local.get([key], result => resolve(result[key]))
    ),
  set: (key, value) =>
    new Promise(resolve =>
      chrome.storage.local.set({ [key]: value }, () => resolve())
    )
};

const wait = milli => new Promise(resolve => setTimeout(resolve, milli));

const fromMicro = micro => {
  // The minimum divisible unit of an Algo is a microAlgo (10^-6).
  // https://www.algorand.com/resources/blog/rewards-technical-overview
  const decimals = new BigNumber(6);
  const divisor = new BigNumber(10).pow(decimals);
  return new BigNumber(micro).div(divisor);
};

const toMicro = algo => {
  const decimals = new BigNumber(6);
  const divisor = new BigNumber(10).pow(decimals);
  return new BigNumber(algo).times(divisor);
};

const formatMicro = micro =>
  fromMicro(micro)
    .toNumber()
    .toLocaleString();

// permissions
const getPermissions = async () => {
  let permissions = await storage.get("permissions");
  if (!permissions) {
    permissions = {};
    await storage.set("permissions", permissions);
  }
  return permissions;
};

const setPermission = async (url, permission, value) => {
  const hostname = new URL(url).hostname;
  const permissions = await getPermissions();
  await storage.set("permissions", {
    ...permissions,
    [hostname]: { [permission]: value }
  });
};

const onlyExtension = (sender, sendResponse) => {
  // only requests originating from this chrome extension are permitted
  const hostname = new URL(sender.url).hostname;
  if (hostname !== window.chrome.runtime.id) {
    sendResponse({ error: "Access Denied" });
    throw new Error("Access Denied");
  }
};

const hasPermission = async (url, permission) => {
  const hostname = new URL(url).hostname;

  const permissions = await getPermissions();
  if (permissions[hostname] && permissions[hostname][permission]) {
    return true;
  }
  return false;
};

let account;
let accountInfo;
let txs;

// extension only

const init = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  // check if user has already logged in for this session
  if (account) {
    return sendResponse({ hasAccount: true, loggedIn: true });
  }

  // check if user has encrypted mnemonic in local storage
  const encryptedMnemonic = await storage.get("mnemonic");
  if (encryptedMnemonic) {
    sendResponse({ hasAccount: true, loggedIn: false });
  } else {
    sendResponse({ hasAccount: false, loggedIn: false });
  }
};

const login = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  try {
    const encryptedMnemonic = await storage.get("mnemonic");
    const bytes = crypto.AES.decrypt(encryptedMnemonic, request.password);
    const mnemonic = bytes.toString(crypto.enc.Utf8);
    account = algosdk.mnemonicToSecretKey(mnemonic);
    sendResponse({ loggedIn: true });
  } catch (e) {
    sendResponse({ loggedIn: false });
  }
};

const logout = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);
  account = null;
  let accountInfo = null;
  let txs = null;
  sendResponse();
};

const generateAccount = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  const account = algosdk.generateAccount();
  const mnemonic = algosdk.secretKeyToMnemonic(account.sk);
  sendResponse({ mnemonic });
};

const createAccount = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  // encrypt mnemonic
  const mnemonic = request.mnemonic;
  const encryptedMnemonic = crypto.AES.encrypt(mnemonic, request.password);
  await storage.set("mnemonic", encryptedMnemonic.toString());

  // save account
  account = algosdk.mnemonicToSecretKey(mnemonic);

  sendResponse({ loggedIn: true });
};

const accountInformation = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  const [info, response] = await Promise.all([
    await algodclient.accountInformation(account.addr),
    await algodclient.transactionByAddress(account.addr, null, null, 10)
  ]);

  const transactions = response.transactions || [];

  // cache response
  accountInfo = info;
  txs = transactions;

  sendResponse({
    ...info,
    transactions
  });
};

const cachedAccountInformation = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  if (accountInfo && txs) {
    sendResponse({
      ...accountInfo,
      transactions: txs
    });
  } else {
    sendResponse();
  }
};

const loadMoreTransactions = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  // fromDate will always be the date of the first block
  const timestampOfFirstBlock = 1560211200000;
  const fromDate = new Date(timestampOfFirstBlock).toISOString();

  // toDate is the date of our last loaded transaction
  const block = await algodclient.block(request.round);
  const toDate = new Date(block.timestamp * 1000).toISOString();

  // max tx per query
  const maxTxs = 10;

  // javascript SDK does not support fromDate / toDate queries yet
  const response = await fetch(
    `${server}:${port}/v1/account/${account.addr}/transactions?fromDate=${fromDate}&toDate=${toDate}&max=${maxTxs}`,
    {
      headers: {
        "x-algo-api-token": apiToken
      }
    }
  );
  const json = await response.json();
  const transactions = json.transactions || [];
  sendResponse(transactions);
};

const validMnemonic = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  try {
    const tempAccount = algosdk.mnemonicToSecretKey(request.mnemonic);
    const isValid = algosdk.isValidAddress(tempAccount.addr);
    sendResponse({ isValid });
  } catch (e) {
    sendResponse({ isValid: false });
  }
};

const notifyWhenTxConfirmed = async txId => {
  let transaction;
  while (!transaction) {
    try {
      transaction = await algodclient.transactionById(txId);
    } catch (e) {}
    await wait(5000);
  }
  window.chrome.notifications.create(
    uuid(),
    {
      iconUrl: chrome.extension.getURL("icon.png"),
      type: "basic",
      title: "Transaction Confirmed",
      message: txId
    },
    () => {}
  );
};

const sendTransaction = async (request, sender, sendResponse) => {
  onlyExtension(sender, sendResponse);

  if (algosdk.isValidAddress(request.to)) {
    const params = await algodclient.getTransactionParams();
    const endRound = params.lastRound + parseInt(1000);

    const txn = {
      from: account.addr,
      to: request.to,
      fee: 1000,
      amount: request.amount,
      firstRound: params.lastRound,
      lastRound: endRound,
      genesisID: params.genesisID,
      genesisHash: params.genesishashb64,
      note: new Uint8Array(0),
      flatFee: true
    };

    // sign the transaction
    const signedTxn = algosdk.signTransaction(txn, account.sk);

    // submit the transaction
    const tx = await algodclient.sendRawTransaction(signedTxn.blob);

    // send notification when transaction confirms
    notifyWhenTxConfirmed(tx.txId);
  }

  sendResponse();
};

// injected apps

const connect = async (request, sender, sendResponse) => {
  if (account) {
    // ensure that the website requesting information has basic permissions
    let granted = await hasPermission(sender.url, "basic");
    if (!granted) {
      granted = window.confirm(
        `${
          new URL(sender.url).hostname
        } would like to:\n\nAccess your account information.\n\nView your transaction history.\n\nPrompt you to make transactions.\n\n`
      );
      if (granted) {
        await setPermission(sender.url, "basic", true);
      }
    }
    if (granted) {
      const info = await algodclient.accountInformation(account.addr);
      sendResponse({ account: info });
    } else {
      sendResponse({ account: null });
    }
  } else {
    sendResponse({ locked: true });
  }
};

const injectedSendTransaction = async (request, sender, sendResponse) => {
  if (account) {
    if (await hasPermission(sender.url, "basic")) {
      if (algosdk.isValidAddress(request.to)) {
        const granted = window.confirm(
          `${
            new URL(sender.url).hostname
          } would like to send the following transaction:\n\nAmount\n${formatMicro(
            request.amount
          )}\n\nTo\n${request.to}\n\n${
            request.note ? `Note\n${JSON.stringify(request.note)}` : ""
          }`
        );

        if (granted) {
          const params = await algodclient.getTransactionParams();
          const endRound = params.lastRound + parseInt(1000);

          const note = request.note
            ? algosdk.encodeObj(request.note)
            : new Uint8Array(0);
          console.log(note);
          const txn = {
            from: account.addr,
            to: request.to,
            fee: 1000,
            amount: request.amount,
            firstRound: params.lastRound,
            lastRound: endRound,
            genesisID: params.genesisID,
            genesisHash: params.genesishashb64,
            note,
            flatFee: true
          };

          try {
            // sign the transaction
            const signedTxn = algosdk.signTransaction(txn, account.sk);

            // submit the transaction
            const tx = await algodclient.sendRawTransaction(signedTxn.blob);
            console.log("txt", tx);
            // send notification when transaction confirms
            notifyWhenTxConfirmed(tx.txId);

            sendResponse(tx.txId);
          } catch (e) {
            return sendResponse({ error: e.text || e.message });
          }
        }
      }
    }
  }
  sendResponse();
};

const handleRequest = async (request, sender, sendResponse) => {
  console.log(request.type);
  switch (request.type) {
    case "INIT":
      init(request, sender, sendResponse);
      break;
    case "GENERATE_ACCOUNT":
      generateAccount(request, sender, sendResponse);
      break;
    case "CREATE_ACCOUNT":
      createAccount(request, sender, sendResponse);
      break;
    case "LOGIN":
      login(request, sender, sendResponse);
      break;
    case "LOGOUT":
      logout(request, sender, sendResponse);
      break;
    case "ACCOUNT_INFO":
      accountInformation(request, sender, sendResponse);
      break;
    case "CACHED_ACCOUNT_INFO":
      cachedAccountInformation(request, sender, sendResponse);
      break;
    case "VALID_MNEMONIC":
      validMnemonic(request, sender, sendResponse);
      break;
    case "LOAD_MORE_TRANSACTIONS":
      loadMoreTransactions(request, sender, sendResponse);
      break;
    case "CONNECT":
      connect(request, sender, sendResponse);
      break;
    case "SEND_TRANSACTION":
      sendTransaction(request, sender, sendResponse);
      break;
    case "INJECTED_SEND_TRANSACTION":
      injectedSendTransaction(request, sender, sendResponse);
      break;
    default:
      break;
  }
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  handleRequest(request, sender, sendResponse);
  return true;
});
