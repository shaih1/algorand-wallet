const sendMessage = payload =>
  new Promise((resolve, reject) => {
    window.chrome.runtime.sendMessage(payload, (response, err) => {
      if (err) {
        return reject(err);
      }
      return resolve(response);
    });
  });

// Content scripts cannot modify the window object of pages
// they are attached to. To expose the window.algo api a provider.js
// script must be injected into the page.

// This injected script does not have access to the chrome extension apis
// so we must use custom events to communicate like this:
// provider.js <-> content.js <-> background.js

// inject provider
const container = document.head || document.documentElement;
const script = document.createElement("script");
script.setAttribute("type", "text/javascript");
script.setAttribute("src", window.chrome.extension.getURL("./provider.js"));
container.insertBefore(script, container.children[0]);

// listen for custom events
window.addEventListener(
  "ALGO_PASS_TO_BACKGROUND",
  async ({ target, detail }) => {
    if (target != window) {
      return;
    }
    const response = await sendMessage(detail.payload);
    const event = new CustomEvent("ALGO_PASS_TO_SCRIPT", {
      detail: { id: detail.id, response }
    });
    window.dispatchEvent(event);
  },
  false
);
